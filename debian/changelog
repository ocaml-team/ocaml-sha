ocaml-sha (1.15.4-3) unstable; urgency=medium

  * Team upload
  * Include ocamlvars.mk in debian/rules

 -- Stéphane Glondu <glondu@debian.org>  Thu, 27 Jul 2023 06:43:04 +0200

ocaml-sha (1.15.4-2) unstable; urgency=medium

  * Team upload
  * Use ocaml_dune DH buildsystem

 -- Stéphane Glondu <glondu@debian.org>  Sat, 15 Jul 2023 12:15:12 +0200

ocaml-sha (1.15.4-1) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * New upstream release

  [ Debian Janitor ]
  * Use correct machine-readable copyright file URI
  * Set upstream metadata fields
  * debian/watch: Use GitHub /tags rather than /releases page
  * Update standards version to 4.6.1

 -- Stéphane Glondu <glondu@debian.org>  Thu, 02 Feb 2023 03:13:06 +0100

ocaml-sha (1.15.2-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Fri, 04 Mar 2022 15:27:02 +0100

ocaml-sha (1.15.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Bump Standards-Version to 4.6.0

 -- Stéphane Glondu <glondu@debian.org>  Mon, 29 Nov 2021 13:10:19 +0100

ocaml-sha (1.13-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update Homepage and debian/watch
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Tue, 18 Aug 2020 09:37:31 +0200

ocaml-sha (1.11-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Add a patch to fix possible GC crash
    - add debian/patches/0007-Fix-a-possible-GC-crash.patch
  * Update Vcs-* fields
  * ocaml-sha has been relicensed under ISC, update d/copyright
    file accordingly

 -- Mehdi Dogguy <mehdi@debian.org>  Fri, 11 May 2018 14:36:20 +0200

ocaml-sha (1.9-2) unstable; urgency=medium

  * Include *.cmx files in package
  * Update Standards-Version and debhelper version
  * Update patches

 -- Eric Cooper <ecc@cmu.edu>  Sun, 23 Jul 2017 18:01:55 -0400

ocaml-sha (1.9-1) unstable; urgency=low

  * New upstream version (closes: #663520)
  * Patch C stubs to silence gcc warnings
  * Update standards version and build dependencies
  * Drop DM-Upload-Allowed
  * Update copyright format
  * Update Homepage and VCS-* fields
  * Increased debhelper compatibility level to 9
  * Update watch file for github

 -- Eric Cooper <ecc@cmu.edu>  Fri, 01 Nov 2013 20:05:22 -0400

ocaml-sha (1.7-2) unstable; urgency=low

  * Add Makefile targets for bytecode-only architectures
    (closes: #616176)

 -- Eric Cooper <ecc@cmu.edu>  Wed, 02 Mar 2011 19:54:10 -0500

ocaml-sha (1.7-1) unstable; urgency=low

  * Use dh instead of cdbs
  * Suggest ocaml-findlib
  * Update standards version and build dependencies
  * Make debian/copyright file machine-readable
  * Improve short descriptions
  * Add debian/source/format
  * Use debian/patches for all changes to upstream source

 -- Eric Cooper <ecc@cmu.edu>  Fri, 25 Feb 2011 14:04:25 -0500

ocaml-sha (1.6-1) unstable; urgency=low

  * New upstream version

 -- Eric Cooper <ecc@cmu.edu>  Fri, 09 Oct 2009 09:59:30 -0400

ocaml-sha (1.5-4) unstable; urgency=low

  * Make install target work correctly on bytecode-only architectures
    (closes: #550298)

 -- Eric Cooper <ecc@cmu.edu>  Thu, 08 Oct 2009 20:44:50 -0400

ocaml-sha (1.5-3) unstable; urgency=low

  * Use DESTDIR with ocamlfind for install target
  * Convert to dh_ocaml version 0.9

 -- Eric Cooper <ecc@cmu.edu>  Wed, 07 Oct 2009 10:21:30 -0400

ocaml-sha (1.5-2) unstable; urgency=low

  * Compile C files with -fPIC (closes: #518038)
  * Change Maintainer to d-o-m
  * Add shasum.ml example in -dev package

 -- Eric Cooper <ecc@cmu.edu>  Tue, 03 Mar 2009 15:43:10 -0500

ocaml-sha (1.5-1) unstable; urgency=low

  * New upstream version
  * Use modified upstream Makefile
  * Add dh-ocaml and drop ocamlmakefile from build-deps

 -- Eric Cooper <ecc@cmu.edu>  Sun, 01 Mar 2009 16:39:29 -0500

ocaml-sha (1.4-6) UNRELEASED; urgency=low

  [ Stefano Zacchiroli ]
  * NOT RELEASED YET

  [ Eric Cooper]
  * Update standards version

  [ Stephane Glondu ]
  * Switching packaging to git

 -- Eric Cooper <ecc@cmu.edu>  Mon, 15 Dec 2008 09:28:10 -0500

ocaml-sha (1.4-5) unstable; urgency=low

  [ Eric Cooper ]
  * Remove XS- prefix from Vcs- fields in debian/control
  * Add DM-Upload-Allowed to debian/control

  [ Stefano Zacchiroli ]
  * promote Homepage to a real debian/control field

 -- Eric Cooper <ecc@cmu.edu>  Sun, 03 Feb 2008 18:00:22 -0500

ocaml-sha (1.4-4) unstable; urgency=low

  * Use support for ocamldoc in OCaml cdbs class

 -- Eric Cooper <ecc@cmu.edu>  Mon, 03 Sep 2007 18:18:40 -0400

ocaml-sha (1.4-3) unstable; urgency=low

  * Changed ${Source-Version} substvar to ${binary:Version} in debian/control

 -- Eric Cooper <ecc@cmu.edu>  Thu, 02 Aug 2007 17:20:31 -0400

ocaml-sha (1.4-2) unstable; urgency=low

  * Replaced dependency on ocaml-tools by ocamlmakefile.
  * Added myself to uploaders.

 -- Ralf Treinen <treinen@debian.org>  Sun, 03 Jun 2007 22:16:25 +0200

ocaml-sha (1.4-1) unstable; urgency=low

  * New upstream version

 -- Eric Cooper <ecc@cmu.edu>  Sun, 22 Apr 2007 12:23:52 -0400

ocaml-sha (1.3-1) unstable; urgency=low

  * New upstream version

 -- Eric Cooper <ecc@cmu.edu>  Sun,  3 Sep 2006 09:13:11 -0400

ocaml-sha (1.2-1) unstable; urgency=low

  * New upstream version (renamed)

 -- Eric Cooper <ecc@cmu.edu>  Thu, 31 Aug 2006 11:00:53 -0400

ocaml-sha1 (0.9-2) unstable; urgency=low

  * Work around incorrect OCamlMakefile libinstall on architectures without
    ocamlopt (ocaml-tools bug #381638), closes: #381620

 -- Eric Cooper <ecc@cmu.edu>  Sun,  6 Aug 2006 14:26:19 -0400

ocaml-sha1 (0.9-1) unstable; urgency=low

  * New upstream version

 -- Eric Cooper <ecc@cmu.edu>  Mon, 31 Jul 2006 12:06:20 -0400

ocaml-sha1 (0.8-2) unstable; urgency=low

  * Add libsha1-ocaml package with runtime stub libraries
  * Convert debian/rules to CDBS

 -- Eric Cooper <ecc@cmu.edu>  Tue, 25 Jul 2006 14:21:30 -0400

ocaml-sha1 (0.8-1) unstable; urgency=low

  * Initial packaging
  * Closes: #378938

 -- Eric Cooper <ecc@cmu.edu>  Sun, 23 Jul 2006 14:30:00 -0400
